# Stoch 2 Gym environment in Bullet3

This repository contains the gym environment for our custom qaudruped robot Stoch 2 build over Bullet3 physics engine. The environment can be used to learn various locomotion behaviors with choice of Reinforcement learning algorithm.

## Note: Robot URDF is not included in the repositry.

# Usage
``stoch_gym_env.py`` contains the main class derived from gym base class with reset() and step() methods.

``stoch.py`` contains basic functions necessary for the high-fidelity simulation and is extensively used in stoch_gym_env file.

``motor.py`` contains the accurate motor model.
